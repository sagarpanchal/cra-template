import { useDispatch, useSelector } from 'react-redux'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { helloActions } from 'redux/reducers/hello'
import { classNames } from 'utils/utils'

import HelloStyles from './hello.module.scss'

export const Hello = () => {
  const helloText = useSelector((state) => state.hello?.text)

  const dispatch = useDispatch()

  const sayHello = (payload) => {
    dispatch(helloActions.sayHello(payload))
  }

  const sayHelloAsync = async (payload) => {
    await new Promise((r) => setTimeout(r, 2000))
    dispatch(helloActions.sayHello(payload))
  }

  const resetText = () => {
    dispatch(helloActions.reset())
  }

  return (
    <div className={classNames(['container-fluid m-3', HelloStyles.Hello])}>
      <button className="btn btn-primary btn-sm" onClick={() => sayHello('Hello, World')}>
        Say hello
      </button>
      <span className="mx-1" />

      <button className="btn btn-secondary btn-sm" onClick={() => sayHelloAsync('Hello, World')}>
        Say hello, after two seconds
      </button>
      <span className="mx-1" />

      <button className="btn btn-danger btn-sm" onClick={() => resetText()}>
        Reset
      </button>
      <hr />

      {helloText?.length > 0 && (
        <h5>
          {helloText} <FontAwesomeIcon icon="hand-peace" />
        </h5>
      )}
    </div>
  )
}
