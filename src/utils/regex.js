export const NUMERIC = {
  STRICT: /^[+-]?[\d]+[.]?[\d]*$/gm,
  LOOSE: /^[+-]?[\d]*[.]?[\d]*$/gm,
}

export const ALPHA_NUMERIC = /^[-0-9a-zA-Z]*$/gm
