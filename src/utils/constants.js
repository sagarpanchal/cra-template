export const FRACTION_LENGTH = 2

export const LOCALE = 'en-US'

export const CURRENCY = 'USD'

export const TIMEZONE_IANA = 'Asia/Kolkata'

export const LUXON_FORMAT = {
  DATE: 'dd/LL/y',
  TIME: 'hh:mm a',
  DATE_TIME: 'dd/LL/y hh:mm a',
  DURATION: 'hh:mm:ss',
}
