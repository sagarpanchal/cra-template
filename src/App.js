import { Hello } from 'components/Hello'

import 'libraries/fontawesome'
import 'sass/app.scss'

function App() {
  return <Hello />
}

export default App
