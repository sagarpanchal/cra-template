import { createAction, createReducer } from '@reduxjs/toolkit'

export const initState = {
  text: '',
}

export const helloActions = {
  sayHello: createAction('@hello/say-hello'),
  reset: createAction('@hello/reset'),
}

const hello = createReducer(initState, (builder) => {
  builder
    .addCase(helloActions.sayHello.type, (state, { payload }) => {
      state.text = payload
    })
    .addCase(helloActions.reset.type, () => {
      return { ...initState }
    })
})

export default hello
