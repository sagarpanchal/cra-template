import { combineReducers, createStore, applyMiddleware } from '@reduxjs/toolkit'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'

import { middleware } from 'redux/middleware'
import { reducers } from 'redux/reducers'

export const store = createStore(
  combineReducers({ ...reducers }),
  composeWithDevTools({ trace: true })(applyMiddleware(...middleware)),
)
